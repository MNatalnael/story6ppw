from django.shortcuts import render
from django.http import HttpResponseRedirect
from . import models, forms

# Create your views here.

def index(request):
    new_status = forms.NewStatus()
    statuses = models.Status.objects.all()

    if request.method == 'POST':
        models.Status.objects.create(
            status = request.POST['status']
        )

        return HttpResponseRedirect("/")

    context = {
        'new_status':new_status,
        'statuses':statuses
    }

    return render(request, 'index.html', context)
