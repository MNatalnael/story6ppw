from django.test import TestCase
from django.urls import resolve, reverse
from story6.views import index

# Create your tests here.
class PostingUnitTest(TestCase):
    def test_index_exist(self):
        response = self.client.get(reverse('story6:index'))
        self.assertEqual(response.status_code, 200)

    def test_index_using_index_template(self):
        response = self.client.get(reverse('story6:index'))
        self.assertTemplateUsed(response, 'index.html')
