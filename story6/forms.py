from django import forms

class NewStatus(forms.Form):
    status = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class':'form-control'
            }
        ),
        label=""

        )
